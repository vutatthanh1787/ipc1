$(document).ready(function(){
	var mHeader = $("#user-header")
		, mBtns = mHeader.find(".m-btn-group")
		, mBtnSns = mBtns.find("#m-sns")
		, mBtnSch = mBtns.find("#m-sch")
		, mSchForm = mHeader.find(".user-search")
		, mBtnNav = mBtns.find("#m-nav");

	// sns
	mBtnSns.on({
		click: function() {
			if(mHeader.hasClass("is-active for-sns")) {
				mHeader.removeClass("is-active for-sns");
			} else if(mHeader.hasClass("is-active for-sch")) {
				mHeader.removeClass("for-sch").addClass("for-sns");
			} else if(mHeader.hasClass("is-active")) {
				mHeader.removeClass("for-sns for-sch").addClass("for-sns");
			} else {
				mHeader.addClass("is-active for-sns");
			}
		}
	});

	// search
	mBtnSch.on({
		click: function() {
			if(mHeader.hasClass("is-active for-sch")) {
				mHeader.removeClass("is-active for-sch");
			} else if(mHeader.hasClass("is-active for-sns")) {
				mHeader.removeClass("for-sns").addClass("for-sch");
				mSchForm.find("input#search").focus();
			} else if(mHeader.hasClass("is-active")) {
				mHeader.removeClass("for-sns for-sch").addClass("for-sch");
				mSchForm.find("input#search").focus();
			} else {
				mHeader.addClass("is-active for-sch");
				mSchForm.find("input#search").focus();
			}
		}
	});

	// nav
	mBtnNav.on({
		click: function() {
			if(mHeader.hasClass("is-active for-sch")) {
				mHeader.removeClass("for-sch");
			} else if(mHeader.hasClass("is-active for-sns")) {
				mHeader.removeClass("for-sns");
			} else if(mHeader.hasClass("is-active")) {
				mHeader.removeClass("is-active");
			} else {
				mHeader.addClass("is-active");
			}
		}
	});
});