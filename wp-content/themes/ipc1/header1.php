<!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/jquery-ui.min.css">
	    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/foundation.min.css">
	    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/custom.foundation.min.css">
	    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/app.css">
	    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/slick.min.css">
	    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/font-awesome.min.css">
	    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/pe.icon7.stroke.min.css">
	    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/ndsoft-fonts.css">
	    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/style.min.css">
	    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/design.style.css">
	    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/menubar.css">
	    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/menubar.css">
	    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

		<!-- <script src="script/jquery.min.js"></script> -->
	    <script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery-3.5.0.min.js"></script>
	    <script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery-ui.min.js"></script>
	    <script src="<?php echo get_template_directory_uri() ?>/assets/js/slick.min.js"></script>
	    <!-- <script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery.rwdImageMaps.min.js"></script> -->
	    <!-- <script src="<?php echo get_template_directory_uri() ?>/assets/js/what-input.js"></script> -->
	    <script src="<?php echo get_template_directory_uri() ?>/assets/js/foundation.min.js"></script>
	    <script src="<?php echo get_template_directory_uri() ?>/assets/js/app.js"></script>
	    <script src="<?php echo get_template_directory_uri() ?>/assets/js/menubar.js"></script>
	    <script src="<?php echo get_template_directory_uri() ?>/assets/js/user.style.js"></script>
	    <script src="<?php echo get_template_directory_uri() ?>/assets/js/resizePhoto.js"></script>
	    <!-- <script src="<?php echo get_template_directory_uri() ?>/assets/js/clipboard.min.js"></script> -->
	    <script src="<?php echo get_template_directory_uri() ?>/assets/js/common.js"></script>

	</head>

	<body>

		<!-- wrap //-->
    <div id="user-wrap" class=" site-bg">
        <div class="row expanded">
            <header id="user-header">
                <div id="user-gnb">
                    <div class="float-center max-width-1080">
                        <div class="user-intro show-for-medium">
                            <?php
								echo sw_get_current_weekday();
							?>
                        </div>
                        <ul class="user-logbox">
                        	<?php pll_the_languages( array(
 
							   'show_flags' => 1,
							   'dropdown' => 0,
							   'show_names' => 0
							 
							)); ?>
                        </ul>
                        <ul>
                            <?php //echo do_shortcode('[gtranslate]'); ?>
                            <?php //echo do_shortcode('[language-switcher]'); ?>
                        </ul>
                    </div>
                </div>
                <div id="nav-header">
                    <div class="float-center max-width-1080">
                        <div class="dis-table">
                            <div class="dis-table-cell user-logo">
                                <a href="#" target="_top">
                                    <!-- Logo mobile -->
                                    <img class="img-logo" style="width: 70px; height: 70px; float: left;" src="images/logo1.png" alt="Logo" />
                                    <div class="sologan">
                                    	<span class="text show-for-medium">BỘ CÔNG THƯƠNG - Cục công thương địa phương</span>
                                    	<span class="text show-for-medium">Trung tâm khuyến công và tư vấn phát triển công nghiệp 1</span>
                                    </div>
                                    
                                </a>
                                <!-- mobile button group //-->
                                <div class="m-btn-group hide-for-large">
                                    <button type="button" id="m-sns" class="btns"><i class="fa fa-share-alt fa-fw"></i><span class="show-for-sr">sns</span></button>
                                    <button type="button" id="m-sch" class="btns"><i class="fa fa-search fa-fw"></i><span class="show-for-sr">Tìm kiếm</span></button>
                                    <button type="button" id="m-nav" class="btns"><i class="fa fa-bars fa-fw"></i><span class="show-for-sr">Danh mục</span></button>
                                </div>
                                <!--// mobile button group -->
                            </div>
                            <div class="dis-table-cell user-banner">
                                <fieldset class="user-search">
                                    <form name="head-search" method="get" action="/news/articleList.html">
                                        <input type="hidden" name="sc_area" value="A">
                                        <input type="hidden" name="view_type" value="sm">
                                        <label class="show-for-sr" for="search">Tìm kiếm</label>
                                        <input type="text" name="sc_word" id="search" onkeydown="" placeholder="" title="search field">
                                        <button type="button" title="search button" onclick=""><i class="fa fa-search fa-fw"></i><span class="show-for-sr">Tìm kiếm</span></button>
                                    </form>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
                
                <nav id="user-nav">
                    <div id="wrapper" class="float-center max-width-1080">
					    <?php //display_primary_menu(); ?>
					</div>
				</nav>

            </header>
