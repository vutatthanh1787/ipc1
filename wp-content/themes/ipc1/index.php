<?php get_header(); ?>
            <!-- container //-->
            <section id="user-container" class="float-center max-width-1080">
                <!-- layer popup position  -->
                <div id="layer-popups" class="float-center width-1080"></div>
                <div class="posi-re float-center width-1080 PC_AD">
                    <div id="floating_banner_left" class="view-aside left-wing" data-scroll="n" data-top-margin="5" data-duration="300">
                    </div>
                    <div id="floating_banner_right" class="view-aside right-wing" data-scroll="n" data-top-margin="5" data-duration="300">
                    </div>
                </div>
                <div class="index-wrap response float-center max-width-1080">
                    <!-- 100% //-->
                    <div class="index-row">
                        <div class="index-columns width-full">
                            <div class="clearfix">
                            </div>
                        </div>
                    </div>
                    <!--// 100% -->
                    <!-- 100% //-->
                    <div class="index-row">
                        <div class="index-columns grid-4 width-full">
                            <!-- 내용 //-->
                            <div class="clearfix">
                            </div>
                            <!--// 내용 -->
                            <div class="index-row">
                                <div class="index-columns grid-1 width-full">
                                    <!-- 내용 //-->
                                    <div class="clearfix">
                                        <article class="box-skin">
                                            <header class="header"></header>
                                            <section class="content">
                                                <div class="auto-article auto-hr07">
                                                    <div id="roll_4" class="auto-for">
                                                        <div class="dis-none">
                                                            <a href="#" target="_top">
                                                                <div class="auto-images cover line" style="background-image:url(images/ninhbuinhmaymoc.jpg)">
                                                                    <span class="show-for-sr">Ninh Bình: Ưu tiên xây dựng mô hình trình diễn kỹ thuật và ứng dụng máy móc</span>
                                                                </div>
                                                                <div class="auto-content">
                                                                    <div class="auto-marbtm-7 auto-fontB">April 22, 2020, 12:18</div>
                                                                    <!-- <div class="size-17 auto-martop-12 auto-marbtm-5 line-height-2-x letter-2 user-point">Maryland Gov. Expresses Thanks to Korea</div> -->
                                                                    <div class="size-25 letter-2 line-height-2-2x auto-fontA onload"><strong>Ninh Bình: Ưu tiên xây dựng mô hình trình diễn kỹ thuật và ứng dụng máy móc</strong></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="dis-none">
                                                            <a href="#" target="_top">
                                                                <div class="auto-images cover line" style="background-image:url(images/backannt.jpg)">
                                                                    <span class="show-for-sr">Khuyến công Bắc Kạn: Nhân tố quan trọng góp phần phát triển công nghiệp địa phương</span>
                                                                </div>
                                                                <div class="auto-content">
                                                                    <div class="auto-marbtm-7 auto-fontB">April 22, 2020, 12:14</div>
                                                                    <!-- <div class="size-17 auto-martop-12 auto-marbtm-5 line-height-2-x letter-2 user-point">To Keep Asiana Afloat until Acquisition by HDC</div> -->
                                                                    <div class="size-25 letter-2 line-height-2-2x auto-fontA onload"><strong>Khuyến công Bắc Kạn: Nhân tố quan trọng góp phần phát triển công nghiệp địa phương</strong></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="dis-none">
                                                            <a href="#" target="_top">
                                                                <div class="auto-images cover line" style="background-image:url(images/hatinhphe.jpg)">
                                                                    <span class="show-for-sr">Hà Tĩnh: Phê duyệt kinh phí khuyến công địa phương năm 2020</span>
                                                                </div>
                                                                <div class="auto-content">
                                                                    <div class="auto-marbtm-7 auto-fontB">April 22, 2020, 11:52</div>
                                                                    <!-- <div class="size-17 auto-martop-12 auto-marbtm-5 line-height-2-x letter-2 user-point">For Violation of Anti-Money Laundering Law</div> -->
                                                                    <div class="size-25 letter-2 line-height-2-2x auto-fontA onload"><strong>Hà Tĩnh: Phê duyệt kinh phí khuyến công địa phương năm 2020</strong></div>
                                                                </div>
                                                            </a>
                                                        </div>


                                                    </div>
                                                    <div id="nav_4" class="auto-nav">
                                                        <div class="dis-none">
                                                            <div class="auto-images cover line height-105" style="background-image:url(images/ninhbuinhmaymoc.jpg)">
                                                                <span class="show-for-sr">Ninh Bình: Ưu tiên xây dựng mô hình trình diễn kỹ thuật và ứng dụng máy móc</span>
                                                            </div>
                                                        </div>
                                                        <div class="dis-none">
                                                            <div class="auto-images cover line height-105" style="background-image:url(images/backannt.jpg)">
                                                                <span class="show-for-sr">Khuyến công Bắc Kạn: Nhân tố quan trọng góp phần phát triển công nghiệp địa phương</span>
                                                            </div>
                                                        </div>
                                                        <div class="dis-none">
                                                            <div class="auto-images cover line height-105" style="background-image:url(images/hatinhphe.jpg)">
                                                                <span class="show-for-sr">Hà Tĩnh: Phê duyệt kinh phí khuyến công địa phương năm 2020</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                <!--
                                                $(document).ready(function() {

                                                    $('#roll_4').slick({
                                                        arrows: true, // 화살표표시
                                                        prevArrow: '<button type="button" class="photo-btn auto-prev"><i class="icon-left"></i><span class="sr-only">prev</span></button>', // 이전버튼
                                                        nextArrow: '<button type="button" class="photo-btn auto-next"><i class="icon-right"></i><span class="sr-only">next</span></button>', // 다음버튼
                                                        dots: false, // pager
                                                        infinite: true, // 무한넘기기
                                                        autoplay: true, // 자동전환
                                                        autoplaySpeed: 10000, // 자동전환속도
                                                        speed: 600, // 넘기는 속도
                                                        asNavFor: '#nav_4'
                                                    });
                                                    $('#nav_4').slick({
                                                        slidesToShow: 3,
                                                        asNavFor: '#roll_4',
                                                        dots: false,
                                                        arrows: false,
                                                        draggable: false,
                                                        focusOnSelect: true
                                                    });

                                                });
                                                //-->
                                                </script>
                                            </section>
                                        </article>
                                        <div class="box-margins height-30"></div>
                                        <article class="box-skin line">
                                            <header class="header"><strong class="user-point">Nổi bật</strong></header>
                                            <section class="content">
                                                <div class="auto-article tiles-3x">
                                                    <ul>
                                                        <li class="auto-columns"><a href="#" target="_top" class="auto-images cover line height-105" style="background-image:url(images/caobangtm.jpg)">
                                                                <span class="show-for-sr">Cao Bằng: Triển khai đề án thế mạnh</span>
                                                            </a>
                                                            <div class="size-14 auto-martop-7"><a href="#" target="_top" class="line-height-3-2x auto-fontA onload">Cao Bằng: Triển khai đề án thế mạnh</a></div>
                                                        </li>
                                                        <li class="auto-columns"><a href="#" target="_top" class="auto-images cover line height-105" style="background-image:url(images/bacgiangphe.jpg)">
                                                                <span class="show-for-sr">Bắc Giang: Phê duyệt Kế hoạch sử dụng kinh phí khuyến công năm 2020</span>
                                                            </a>
                                                            <div class="size-14 auto-martop-7"><a href="#" target="_top" class="line-height-3-2x auto-fontA onload">Bắc Giang: Phê duyệt Kế hoạch sử dụng kinh phí khuyến công năm 2020</a></div>
                                                        </li>
                                                        <li class="auto-columns"><a href="#" target="_top" class="auto-images cover line height-105" style="background-image:url(images/hanoi28ty.jpg)">
                                                                <span class="show-for-sr">Hà Nội sẽ hỗ trợ 28 tỷ đồng cho hoạt động khuyến công năm 2020</span>
                                                            </a>
                                                            <div class="size-14 auto-martop-7"><a href="#" target="_top" class="line-height-3-2x auto-fontA onload">Hà Nội sẽ hỗ trợ 28 tỷ đồng cho hoạt động khuyến công năm 2020</a></div>
                                                        </li>

                                                    </ul>
                                                </div>
                                            </section>
                                        </article>
                                        <div class="box-margins height-10"></div>
                                    </div>
                                    <!--// 내용 -->
                                </div>
                                <div class="index-columns grid-2 width-260">
                                    <!-- 내용 //-->
                                    <div class="clearfix">
                                        <article class="box-skin line pd_0" style="max-height: 800px; height: 800px">
                                            <header class="header" style="padding: 2px; background-color: #1e218a; text-align: center; line-height: 1em;"><strong class="user-point" style="color: #ffffff !important; margin: 0; padding: 8px;">KHUYẾN CÔNG QUỐC GIA</strong></header>
                                            <section class="content">
                                                <div class="auto-article auto-d04">
                                                    <ul>
                                                        <li class="clearfix">
                                                            <a href="/news/articleView.html?idxno=44642" target="_top" class="size-14 line-height-3-3x auto-fontA onload"><strong class="user-point">Quảng ninh: Điều chỉnh chính sách xã hội: </strong>Chỉ 1 đồng vốn hỗ trợ thu hút tới hơn 15 đồng vốn đối ứng của đối tượng thụ hưởng</a>
                                                        </li>
                                                        <li class="clearfix auto-martop-15 auto-padtop-15 auto-dot">
                                                            <a href="/news/articleView.html?idxno=44642" target="_top" class="size-14 line-height-3-3x auto-fontA onload"><strong class="user-point">Quảng ninh: Điều chỉnh chính sách xã hội: </strong>Chỉ 1 đồng vốn hỗ trợ thu hút tới hơn 15 đồng vốn đối ứng của đối tượng thụ hưởng</a>
                                                        </li>
                                                        <li class="clearfix auto-martop-15 auto-padtop-15 auto-dot">
                                                            <a href="/news/articleView.html?idxno=44642" target="_top" class="size-14 line-height-3-3x auto-fontA onload"><strong class="user-point">Quảng ninh: Điều chỉnh chính sách xã hội: </strong>Chỉ 1 đồng vốn hỗ trợ thu hút tới hơn 15 đồng vốn đối ứng của đối tượng thụ hưởng</a>
                                                        </li>
                                                        <li class="clearfix auto-martop-15 auto-padtop-15 auto-dot">
                                                            <a href="/news/articleView.html?idxno=44642" target="_top" class="size-14 line-height-3-3x auto-fontA onload"><strong class="user-point">Quảng ninh: Điều chỉnh chính sách xã hội: </strong>Chỉ 1 đồng vốn hỗ trợ thu hút tới hơn 15 đồng vốn đối ứng của đối tượng thụ hưởng</a>
                                                        </li>
                                                        <li class="clearfix auto-martop-15 auto-padtop-15 auto-dot">
                                                            <a href="/news/articleView.html?idxno=44642" target="_top" class="size-14 line-height-3-3x auto-fontA onload"><strong class="user-point">Quảng ninh: Điều chỉnh chính sách xã hội: </strong>Chỉ 1 đồng vốn hỗ trợ thu hút tới hơn 15 đồng vốn đối ứng của đối tượng thụ hưởng</a>
                                                        </li>
                                                        <li class="clearfix auto-martop-15 auto-padtop-15 auto-dot">
                                                            <a href="/news/articleView.html?idxno=44642" target="_top" class="size-14 line-height-3-3x auto-fontA onload"><strong class="user-point">Quảng ninh: Điều chỉnh chính sách xã hội: </strong>Chỉ 1 đồng vốn hỗ trợ thu hút tới hơn 15 đồng vốn đối ứng của đối tượng thụ hưởng</a>
                                                        </li>
                                                        <li class="clearfix auto-martop-15 auto-padtop-15 auto-dot">
                                                            <a href="/news/articleView.html?idxno=44642" target="_top" class="size-14 line-height-3-3x auto-fontA onload"><strong class="user-point">Quảng ninh: Điều chỉnh chính sách xã hội: </strong>Chỉ 1 đồng vốn hỗ trợ thu hút tới hơn 15 đồng vốn đối ứng của đối tượng thụ hưởng</a>
                                                        </li>
                                                        <li class="clearfix auto-martop-15 auto-padtop-15 auto-dot">
                                                            <a href="/news/articleView.html?idxno=44642" target="_top" class="size-14 line-height-3-3x auto-fontA onload"><strong class="user-point">Quảng ninh: Điều chỉnh chính sách xã hội: </strong>Chỉ 1 đồng vốn hỗ trợ thu hút tới hơn 15 đồng vốn đối ứng của đối tượng thụ hưởng</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </section>
                                        </article>
                                        <div class="box-margins height-10"></div>
                                    </div>
                                    <!--// 내용 -->
                                </div>
                            </div>
                            <!-- 내용 //-->
                            <div class="clearfix">
                            </div>
                            <!--// 내용 -->
                        </div>
                        <div class="index-columns grid-3 width-310" style="max-height: 800px; overflow: hidden; height: 800px">
                            <div class="clearfix">
                            	<div class="pdf-widget user-bg" style="background-color: #ffffff !important">
                                    <div class="titles" style="background-color: #1e218a">Video</div>
                                    <div class="content" style="padding: 10px 0">
                                        <iframe width="280" height="200" src="https://www.youtube.com/embed/4MOgex0mqoY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                </div>
                                <div style="padding: 5px 0"></div>
                                <div class="pdf-widget user-bg" style="background-color: #ffffff !important">
                                    <div class="titles" style="background-color: #1e218a">Liên kết websites</div>
                                    <div class="content">
                                        <a href="#">
                                            <div class="images">
                                                <img src="images/banner-dkde.png" alt="cover image 394" class="" />
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="images">
                                                <img src="images/lk1.png" alt="cover image 394" class="" />
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="images">
                                                <img src="images/lk2.png" alt="cover image 394" class="" />
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div style="padding: 5px 0"></div>
                                <div class="pdf-widget user-bg" style="background-color: #ffffff !important; overflow: hidden;">
                                    <div class="titles" style="background-color: #1e218a">Thời tiết</div>
                                    <div class="content">
                                        <iframe frameborder="0" marginwidth="0" marginheight="0" src="http://thienduongweb.com/tool/weather/?r=1&w=1&g=1&col=1&d=0" width="100%" height="180" scrolling="no"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--// 100% -->
                    <!-- 100% //-->
                    
                    <!--// 100% -->
                    <!-- 100% //-->
                    <div class="index-row">
                        <div class="index-columns grid-4 width-full" style="padding-right: 0">
                            <!-- 내용 //-->
                            <div class="clearfix">
                                <div class="responsive-banner auto-marbtm-15">
                                    
                                    
                                </div>

                                <div class="main-boxs">
                                    <!-- Tin tức và sự kiện -->
                                    <div class="boxs-cell">
                                        <article class="box-skin line header-line">
                                            <header class="header"><a href="#" class="custom-box-titles user-point">Tin tổng hợp</a></header>
                                            <section class="content">
                                                <div class="auto-article auto-boll auto-dl05">
                                                    <ul>
                                                        <li class="auto-reset clearfix">
                                                            <a href="#" target="_top">
                                                                <div class="auto-images cover line" style="background-image:url(images/tuvan.jpg)">
                                                                    <span class="show-for-sr">Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</span>
                                                                </div>
                                                                <div class="size-19 line-height-3-2x auto-fontA onload"><strong>Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</strong></div>
                                                                <div class="auto-martop-12 size-13 line-height-2-x auto-fontA">April 22, 2020, 14:41</div>
                                                                <p class="auto-martop-8 line-height-4-3x auto-fontA">Bộ trưởng Bộ Công Thương Trần Tuấn Anh vừa ký Quyết định số 842/QĐ-BCT ngày 16/3/2020 về việc ban hành Quy định...</p>
                                                            </a>
                                                        </li>
                                                        <li class="clearfix auto-martop-12 auto-padtop-12 auto-dot"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Tăng năng suất nhờ công nghệ 4.0</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Hướng dẫn biện pháp tự vệ đối với hàng dệt may trong Hiệp định CPTPP</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Thương chiến Mỹ - Trung, dệt may Việt Nam tăng thị phần xuất khẩu</a></li>
                                                    </ul>
                                                </div>
                                            </section>
                                        </article>
                                    </div>
                                    <div class="boxs-cell">
                                        <article class="box-skin line header-line">
                                            <header class="header"><a href="#" class="custom-box-titles user-point">Tin hoạt động</a></header>
                                            <section class="content">
                                                <div class="auto-article auto-boll auto-dl05">
                                                    <ul>
                                                        <li class="auto-reset clearfix">
                                                            <a href="#" target="_top">
                                                                <div class="auto-images cover line" style="background-image:url(images/tuvan.jpg)">
                                                                    <span class="show-for-sr">Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</span>
                                                                </div>
                                                                <div class="size-19 line-height-3-2x auto-fontA onload"><strong>Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</strong></div>
                                                                <div class="auto-martop-12 size-13 line-height-2-x auto-fontA">April 22, 2020, 14:41</div>
                                                                <p class="auto-martop-8 line-height-4-3x auto-fontA">Bộ trưởng Bộ Công Thương Trần Tuấn Anh vừa ký Quyết định số 842/QĐ-BCT ngày 16/3/2020 về việc ban hành Quy định...</p>
                                                            </a>
                                                        </li>
                                                        <li class="clearfix auto-martop-12 auto-padtop-12 auto-dot"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Tăng năng suất nhờ công nghệ 4.0</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Hướng dẫn biện pháp tự vệ đối với hàng dệt may trong Hiệp định CPTPP</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Thương chiến Mỹ - Trung, dệt may Việt Nam tăng thị phần xuất khẩu</a></li>
                                                    </ul>
                                                </div>
                                            </section>
                                        </article>
                                    </div>
                                    <div class="boxs-cell">
                                        <article class="box-skin line header-line">
                                            <header class="header"><a href="#" class="custom-box-titles user-point">Tin địa phương</a></header>
                                            <section class="content">
                                                <div class="auto-article auto-boll auto-dl05">
                                                    <ul>
                                                        <li class="auto-reset clearfix">
                                                            <a href="#" target="_top">
                                                                <div class="auto-images cover line" style="background-image:url(images/tuvan.jpg)">
                                                                    <span class="show-for-sr">Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</span>
                                                                </div>
                                                                <div class="size-19 line-height-3-2x auto-fontA onload"><strong>Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</strong></div>
                                                                <div class="auto-martop-12 size-13 line-height-2-x auto-fontA">April 22, 2020, 14:41</div>
                                                                <p class="auto-martop-8 line-height-4-3x auto-fontA">Bộ trưởng Bộ Công Thương Trần Tuấn Anh vừa ký Quyết định số 842/QĐ-BCT ngày 16/3/2020 về việc ban hành Quy định...</p>
                                                            </a>
                                                        </li>
                                                        <li class="clearfix auto-martop-12 auto-padtop-12 auto-dot"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Tăng năng suất nhờ công nghệ 4.0</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Hướng dẫn biện pháp tự vệ đối với hàng dệt may trong Hiệp định CPTPP</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Thương chiến Mỹ - Trung, dệt may Việt Nam tăng thị phần xuất khẩu</a></li>
                                                    </ul>
                                                </div>
                                            </section>
                                        </article>
                                    </div>
                                    <!-- Tư vấn -->
                                    <div class="boxs-cell">
                                        <article class="box-skin line header-line">
                                            <header class="header"><a href="#" class="custom-box-titles user-point">Tư vấn</a></header>
                                            <section class="content">
                                                <div class="auto-article auto-boll auto-dl05">
                                                    <ul>
                                                        <li class="auto-reset clearfix">
                                                            <a href="#" target="_top">
                                                                <div class="auto-images cover line" style="background-image:url(images/tuvan.jpg)">
                                                                    <span class="show-for-sr">Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</span>
                                                                </div>
                                                                <div class="size-19 line-height-3-2x auto-fontA onload"><strong>Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</strong></div>
                                                                <div class="auto-martop-12 size-13 line-height-2-x auto-fontA">April 22, 2020, 14:41</div>
                                                                <p class="auto-martop-8 line-height-4-3x auto-fontA">Bộ trưởng Bộ Công Thương Trần Tuấn Anh vừa ký Quyết định số 842/QĐ-BCT ngày 16/3/2020 về việc ban hành Quy định...</p>
                                                            </a>
                                                        </li>
                                                        <li class="clearfix auto-martop-12 auto-padtop-12 auto-dot"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Tăng năng suất nhờ công nghệ 4.0</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Hướng dẫn biện pháp tự vệ đối với hàng dệt may trong Hiệp định CPTPP</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Thương chiến Mỹ - Trung, dệt may Việt Nam tăng thị phần xuất khẩu</a></li>
                                                    </ul>
                                                </div>
                                            </section>
                                        </article>
                                    </div>
                                    <div class="boxs-cell">
                                        <article class="box-skin line header-line">
                                            <header class="header"><a href="#" class="custom-box-titles user-point">Giao thương</a></header>
                                            <section class="content">
                                                <div class="auto-article auto-boll auto-dl05">
                                                    <ul>
                                                        <li class="auto-reset clearfix">
                                                            <a href="#" target="_top">
                                                                <div class="auto-images cover line" style="background-image:url(images/tuvan.jpg)">
                                                                    <span class="show-for-sr">Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</span>
                                                                </div>
                                                                <div class="size-19 line-height-3-2x auto-fontA onload"><strong>Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</strong></div>
                                                                <div class="auto-martop-12 size-13 line-height-2-x auto-fontA">April 22, 2020, 14:41</div>
                                                                <p class="auto-martop-8 line-height-4-3x auto-fontA">Bộ trưởng Bộ Công Thương Trần Tuấn Anh vừa ký Quyết định số 842/QĐ-BCT ngày 16/3/2020 về việc ban hành Quy định...</p>
                                                            </a>
                                                        </li>
                                                        <li class="clearfix auto-martop-12 auto-padtop-12 auto-dot"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Tăng năng suất nhờ công nghệ 4.0</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Hướng dẫn biện pháp tự vệ đối với hàng dệt may trong Hiệp định CPTPP</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Thương chiến Mỹ - Trung, dệt may Việt Nam tăng thị phần xuất khẩu</a></li>
                                                    </ul>
                                                </div>
                                            </section>
                                        </article>
                                    </div>
                                    <div class="boxs-cell">
                                        <article class="box-skin line header-line">
                                            <header class="header"><a href="#" class="custom-box-titles user-point">Tiết kiệm năng lượng</a></header>
                                            <section class="content">
                                                <div class="auto-article auto-boll auto-dl05">
                                                    <ul>
                                                        <li class="auto-reset clearfix">
                                                            <a href="#" target="_top">
                                                                <div class="auto-images cover line" style="background-image:url(images/tuvan.jpg)">
                                                                    <span class="show-for-sr">Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</span>
                                                                </div>
                                                                <div class="size-19 line-height-3-2x auto-fontA onload"><strong>Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</strong></div>
                                                                <div class="auto-martop-12 size-13 line-height-2-x auto-fontA">April 22, 2020, 14:41</div>
                                                                <p class="auto-martop-8 line-height-4-3x auto-fontA">Bộ trưởng Bộ Công Thương Trần Tuấn Anh vừa ký Quyết định số 842/QĐ-BCT ngày 16/3/2020 về việc ban hành Quy định...</p>
                                                            </a>
                                                        </li>
                                                        <li class="clearfix auto-martop-12 auto-padtop-12 auto-dot"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Tăng năng suất nhờ công nghệ 4.0</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Hướng dẫn biện pháp tự vệ đối với hàng dệt may trong Hiệp định CPTPP</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Thương chiến Mỹ - Trung, dệt may Việt Nam tăng thị phần xuất khẩu</a></li>
                                                    </ul>
                                                </div>
                                            </section>
                                        </article>
                                    </div>
                                    <div class="boxs-cell">
                                        <article class="box-skin line header-line">
                                            <header class="header"><a href="#" class="custom-box-titles user-point">Sản xuất sạch hơn</a></header>
                                            <section class="content">
                                                <div class="auto-article auto-boll auto-dl05">
                                                    <ul>
                                                        <li class="auto-reset clearfix">
                                                            <a href="#" target="_top">
                                                                <div class="auto-images cover line" style="background-image:url(images/tuvan.jpg)">
                                                                    <span class="show-for-sr">Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</span>
                                                                </div>
                                                                <div class="size-19 line-height-3-2x auto-fontA onload"><strong>Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</strong></div>
                                                                <div class="auto-martop-12 size-13 line-height-2-x auto-fontA">April 22, 2020, 14:41</div>
                                                                <p class="auto-martop-8 line-height-4-3x auto-fontA">Bộ trưởng Bộ Công Thương Trần Tuấn Anh vừa ký Quyết định số 842/QĐ-BCT ngày 16/3/2020 về việc ban hành Quy định...</p>
                                                            </a>
                                                        </li>
                                                        <li class="clearfix auto-martop-12 auto-padtop-12 auto-dot"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Tăng năng suất nhờ công nghệ 4.0</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Hướng dẫn biện pháp tự vệ đối với hàng dệt may trong Hiệp định CPTPP</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Thương chiến Mỹ - Trung, dệt may Việt Nam tăng thị phần xuất khẩu</a></li>
                                                    </ul>
                                                </div>
                                            </section>
                                        </article>
                                    </div>
                                    <!-- Công nghiệp nông thôn -->
                                    <div class="boxs-cell">
                                        <article class="box-skin line header-line">
                                            <header class="header"><a href="#" class="custom-box-titles user-point">Công nghiệp nông thôn</a></header>
                                            <section class="content">
                                                <div class="auto-article auto-boll auto-dl05">
                                                    <ul>
                                                        <li class="auto-reset clearfix">
                                                            <a href="#" target="_top">
                                                                <div class="auto-images cover line" style="background-image:url(images/tuvan.jpg)">
                                                                    <span class="show-for-sr">Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</span>
                                                                </div>
                                                                <div class="size-19 line-height-3-2x auto-fontA onload"><strong>Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</strong></div>
                                                                <div class="auto-martop-12 size-13 line-height-2-x auto-fontA">April 22, 2020, 14:41</div>
                                                                <p class="auto-martop-8 line-height-4-3x auto-fontA">Bộ trưởng Bộ Công Thương Trần Tuấn Anh vừa ký Quyết định số 842/QĐ-BCT ngày 16/3/2020 về việc ban hành Quy định...</p>
                                                            </a>
                                                        </li>
                                                        <li class="clearfix auto-martop-12 auto-padtop-12 auto-dot"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Tăng năng suất nhờ công nghệ 4.0</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Hướng dẫn biện pháp tự vệ đối với hàng dệt may trong Hiệp định CPTPP</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Thương chiến Mỹ - Trung, dệt may Việt Nam tăng thị phần xuất khẩu</a></li>
                                                    </ul>
                                                </div>
                                            </section>
                                        </article>
                                    </div>
                                    <!-- Văn bản pháp quy -->
                                    <div class="boxs-cell">
                                        <article class="box-skin line header-line">
                                            <header class="header"><a href="#" class="custom-box-titles user-point">Văn bản khuyến công</a></header>
                                            <section class="content">
                                                <div class="auto-article auto-boll auto-dl05">
                                                    <ul>
                                                        <li class="auto-reset clearfix">
                                                            <a href="#" target="_top">
                                                                <div class="auto-images cover line" style="background-image:url(images/tuvan.jpg)">
                                                                    <span class="show-for-sr">Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</span>
                                                                </div>
                                                                <div class="size-19 line-height-3-2x auto-fontA onload"><strong>Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</strong></div>
                                                                <div class="auto-martop-12 size-13 line-height-2-x auto-fontA">April 22, 2020, 14:41</div>
                                                                <p class="auto-martop-8 line-height-4-3x auto-fontA">Bộ trưởng Bộ Công Thương Trần Tuấn Anh vừa ký Quyết định số 842/QĐ-BCT ngày 16/3/2020 về việc ban hành Quy định...</p>
                                                            </a>
                                                        </li>
                                                        <li class="clearfix auto-martop-12 auto-padtop-12 auto-dot"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Tăng năng suất nhờ công nghệ 4.0</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Hướng dẫn biện pháp tự vệ đối với hàng dệt may trong Hiệp định CPTPP</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Thương chiến Mỹ - Trung, dệt may Việt Nam tăng thị phần xuất khẩu</a></li>
                                                    </ul>
                                                </div>
                                            </section>
                                        </article>
                                    </div>
                                    <div class="boxs-cell">
                                        <article class="box-skin line header-line">
                                            <header class="header"><a href="#" class="custom-box-titles user-point">Văn bản chung</a></header>
                                            <section class="content">
                                                <div class="auto-article auto-boll auto-dl05">
                                                    <ul>
                                                        <li class="auto-reset clearfix">
                                                            <a href="#" target="_top">
                                                                <div class="auto-images cover line" style="background-image:url(images/tuvan.jpg)">
                                                                    <span class="show-for-sr">Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</span>
                                                                </div>
                                                                <div class="size-19 line-height-3-2x auto-fontA onload"><strong>Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</strong></div>
                                                                <div class="auto-martop-12 size-13 line-height-2-x auto-fontA">April 22, 2020, 14:41</div>
                                                                <p class="auto-martop-8 line-height-4-3x auto-fontA">Bộ trưởng Bộ Công Thương Trần Tuấn Anh vừa ký Quyết định số 842/QĐ-BCT ngày 16/3/2020 về việc ban hành Quy định...</p>
                                                            </a>
                                                        </li>
                                                        <li class="clearfix auto-martop-12 auto-padtop-12 auto-dot"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Tăng năng suất nhờ công nghệ 4.0</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Hướng dẫn biện pháp tự vệ đối với hàng dệt may trong Hiệp định CPTPP</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Thương chiến Mỹ - Trung, dệt may Việt Nam tăng thị phần xuất khẩu</a></li>
                                                    </ul>
                                                </div>
                                            </section>
                                        </article>
                                    </div>

                                    <div class="boxs-cell">
                                        <article class="box-skin line header-line">
                                            <header class="header"><a href="#" class="custom-box-titles user-point">Thông tin doanh nghiệp & sản phẩm</a></header>
                                            <section class="content">
                                                <div class="auto-article auto-boll auto-dl05">
                                                    <ul>
                                                        <li class="auto-reset clearfix">
                                                            <a href="#" target="_top">
                                                                <div class="auto-images cover line" style="background-image:url(images/tuvan.jpg)">
                                                                    <span class="show-for-sr">Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</span>
                                                                </div>
                                                                <div class="size-19 line-height-3-2x auto-fontA onload"><strong>Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</strong></div>
                                                                <div class="auto-martop-12 size-13 line-height-2-x auto-fontA">April 22, 2020, 14:41</div>
                                                                <p class="auto-martop-8 line-height-4-3x auto-fontA">Bộ trưởng Bộ Công Thương Trần Tuấn Anh vừa ký Quyết định số 842/QĐ-BCT ngày 16/3/2020 về việc ban hành Quy định...</p>
                                                            </a>
                                                        </li>
                                                        <li class="clearfix auto-martop-12 auto-padtop-12 auto-dot"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Tăng năng suất nhờ công nghệ 4.0</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Hướng dẫn biện pháp tự vệ đối với hàng dệt may trong Hiệp định CPTPP</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Thương chiến Mỹ - Trung, dệt may Việt Nam tăng thị phần xuất khẩu</a></li>
                                                    </ul>
                                                </div>
                                            </section>
                                        </article>
                                    </div>
                                    
                                    <div class="boxs-cell">
                                        <article class="box-skin line header-line">
                                            <header class="header"><a href="#" class="custom-box-titles user-point">Công nghiệp nông thôn</a></header>
                                            <section class="content">
                                                <div class="auto-article auto-boll auto-dl05">
                                                    <ul>
                                                        <li class="auto-reset clearfix">
                                                            <a href="#" target="_top">
                                                                <div class="auto-images cover line" style="background-image:url(images/tuvan.jpg)">
                                                                    <span class="show-for-sr">Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</span>
                                                                </div>
                                                                <div class="size-19 line-height-3-2x auto-fontA onload"><strong>Bộ Công Thương: Ban hành quy định về mạng lưới tư vấn viên hỗ trợ doanh nghiệp nhỏ và vừa ngành Công Thương</strong></div>
                                                                <div class="auto-martop-12 size-13 line-height-2-x auto-fontA">April 22, 2020, 14:41</div>
                                                                <p class="auto-martop-8 line-height-4-3x auto-fontA">Bộ trưởng Bộ Công Thương Trần Tuấn Anh vừa ký Quyết định số 842/QĐ-BCT ngày 16/3/2020 về việc ban hành Quy định...</p>
                                                            </a>
                                                        </li>
                                                        <li class="clearfix auto-martop-12 auto-padtop-12 auto-dot"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Tăng năng suất nhờ công nghệ 4.0</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Hướng dẫn biện pháp tự vệ đối với hàng dệt may trong Hiệp định CPTPP</a></li>
                                                        <li class="clearfix auto-martop-6"><a href="#" target="_top" class="size-14 line-height-3-x auto-fontA onload">Thương chiến Mỹ - Trung, dệt may Việt Nam tăng thị phần xuất khẩu</a></li>
                                                    </ul>
                                                </div>
                                            </section>
                                        </article>
                                    </div>
                                </div>
                                <div class="box-margins height-25"></div>
                            </div>
                            <!--// 내용 -->
                            <div class="index-row">
                                <div class="index-columns grid-1 width-full">
                                    <!-- 내용 //-->
                                    <div class="clearfix">
                                    </div>
                                    <!--// 내용 -->
                                </div>
                                <div class="index-columns grid-2 width-260">
                                    <!-- 내용 //-->
                                    <div class="clearfix">
                                    </div>
                                    <!--// 내용 -->
                                </div>
                            </div>
                            <!-- 내용 //-->
                            <div class="clearfix">
                            </div>
                            <!--// 내용 -->
                        </div>
                        
                    </div>
                    
                    <div class="index-row">
                        <div class="index-columns width-full">
                            <!-- 내용 //-->
                            <div class="clearfix">
                                <article class="box-skin header-bg">
                                    <header class="header" style="margin-bottom:0;background-color:#231f20"><strong class="custom-box-titles white">Doanh nghiệp tiêu biểu</strong></header>
                                    <section class="content">
                                        <div id="roll_6" class="auto-article auto-pr08">
                                            <div class="dis-none auto-columns">
                                                <a href="#" target="_top" style="background-image:url(images/bcthtcnnt1.jpg)"></a>
                                            </div>
                                            <div class="dis-none auto-columns">
                                                <a href="#" target="_top" style="background-image:url(images/hieuquadakc2.jpg)">
                                                </a>
                                            </div>
                                            <div class="dis-none auto-columns">
                                                <a href="#" target="_top" style="background-image:url(images/hieuquadakcqg1.jpg)">
                                                </a>
                                            </div>
                                            <div class="dis-none auto-columns">
                                                <a href="#" target="_top" style="background-image:url(images/haiphongkcqg.jpg)">
                                                </a>
                                            </div>
                                            <div class="dis-none auto-columns">
                                                <a href="#" target="_top" style="background-image:url(images/nguyenvanmanh1.jpg)">
                                                </a>
                                            </div>
                                            <div class="dis-none auto-columns">
                                                <a href="#" target="_top" style="background-image:url(images/ninhbinhnghiem.jpg)">
                                                </a>
                                            </div>
                                            <div class="dis-none auto-columns">
                                                <a href="#" target="_top" style="background-image:url(images/resized_minhhong1.jpg)">
                                                </a>
                                            </div>
                                            <div class="dis-none auto-columns">
                                                <a href="#" target="_top" style="background-image:url(images/resized_minhhong1.jpg)">
                                                </a>
                                            </div>
                                            <div class="dis-none auto-columns">
                                                <a href="#" target="_top" style="background-image:url(images/hotrocbns3.jpg)">
                                                </a>
                                            </div>
                                            <div class="dis-none auto-columns">
                                                <a href="#" target="_top" style="background-image:url(images/hotrocbns3.jpg)">
                                                </a>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                        <!--
                                        $(document).ready(function() {

                                            $('#roll_6').slick({
                                                slidesToShow: 5, // 보이는갯수
                                                arrows: true, // 화살표표시
                                                prevArrow: '<button type="button" class="photo-btn auto-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i><span class="sr-only">prev</span></button>', // 이전버튼
                                                nextArrow: '<button type="button" class="photo-btn auto-next"><i class="fa fa-chevron-right" aria-hidden="true"></i><span class="sr-only">next</span></button>', // 다음버튼
                                                dots: false, // pager
                                                infinite: true, // 무한넘기기
                                                autoplay: true, // 자동전환
                                                //autoplaySpeed: 3000, // 자동전환속도
                                                speed: 300, // 넘기는 속도
                                                centerMode: false, // 가운데정렬
                                                responsive: [{
                                                        breakpoint: 1024,
                                                        settings: {
                                                            slidesToShow: 3, // 보이는갯수
                                                            slidesToScroll: 3
                                                        }
                                                    },
                                                    {
                                                        breakpoint: 640,
                                                        settings: {
                                                            slidesToShow: 2, // 보이는갯수
                                                            slidesToScroll: 2
                                                        }
                                                    }
                                                ]
                                            });
                                        });
                                        //-->
                                        </script>
                                    </section>
                                </article>
                                <div class="box-margins height-25"></div>
                            </div>
                            <!--// 내용 -->
                        </div>
                    </div>
                </div>
            </section>
            <!--// container -->
            <!-- footer //-->
            <footer id="user-footer" class="response">
                <div class="float-center max-width-1080">
                    <!-- nav //-->
                    <nav class="footer-nav">
                        <a href="#">Trang chủ</a>
                        <a href="#" class="show-for-medium">Thông báo</a>
                        <a href="#" class="show-for-medium">Giới thiệu</a>
                        <a href="#" class="show-for-medium">Tin tức</a>
                        <a href="#" class="show-for-medium">RSS</a>
                    </nav>
                    <!--// nav -->
                    <!-- content //-->
                    <div class="footer-content">
                        <div class="footer-cell">
                        	<?php 

								$args = array(
								    'post_type'=> 'thong_tin_website',
							    );              

								$the_query = new WP_Query( $args );
								if($the_query->have_posts() ) {
									while ($the_query->have_posts()) {
										$the_query->the_post();

										the_content();
									}
								}

							?>
                        </div>
                        <div class="footer-cell small">
                            <!-- sns //-->
                            <div class="user-sns">
                                <div class="titles">Social Links</div>
                                <ul class="lists">
                                    <li><a href="#" target="_blank"><i class="fa fa-facebook fa-fw"></i> <span class="text">Facebook</span></a></li>
                                    <li><a href="#" target="_blank"><i class="fa fa-twitter fa-fw"></i> <span class="text">Twitter</span></a></li>
                                    <li><a href="#" target="_blank"><i class="fa fa-google-plus fa-fw"></i> <span class="text">Google+</span></a></li>
                                    <li><a href="#" target="_blank"><i class="fa fa-linkedin fa-fw"></i> <span class="text">Linkedin</span></a></li>
                                    <li><a href="#" target="_top"><i class="fa fa-rss fa-fw"></i> <span class="text">RSS</span></a></li>
                                </ul>
                            </div>
                            <!--// sns -->
                        </div>
                    </div>
                    <!--// content -->
                </div>
            </footer>
            <!--// footer -->
        </div>
    </div>
    <!--// wrap -->
    <?php wp_footer(); ?>
    <!-- back to the top //-->
    <button type="button" id="back-to-top" class="back-to-top for-mobile" title="Top"><i class="fa fa-angle-up" aria-hidden="true"></i><span class="show-for-sr">Top</span></button>
    <!--// back to the top -->
    <script type="text/javascript">
    <!--
    // back to the top      
    $(document).scroll(function() {
        var sDocumentHeight = $(document).height() - $(window).height();
        if ($(document).scrollTop() > 0) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });

    $('#back-to-top').click(function() {
        $('html, body').animate({ scrollTop: 0 }, 100);
    });
    //-->
    </script>

</body>

</html>